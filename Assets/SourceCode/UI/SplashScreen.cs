using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UIFramework;
public class SplashScreen : ScreenBase
{
    [SerializeField] private float m_splashLoadingTime = 1f;
    public override void OnShow()
    {
        base.OnShow();

        StartCoroutine(CoroutineUtils.Delay(m_splashLoadingTime, ShowMainMenu));
    }


    private void ShowMainMenu()
    {
        ScreenFactory.Instance.Deactivate<SplashScreen>();
        ScreenFactory.Instance.Activate<MainMenuScreen>();
    }

}
