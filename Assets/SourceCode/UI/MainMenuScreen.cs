
using UIFramework;
public class MainMenuScreen : ScreenBase
{
    private const string PLAY_BTN = "Btn-Play";

    public override void OnButtonClick(string btnName)
    {

        switch (btnName)
        {
            case PLAY_BTN:

                ScreenFactory.Instance.Deactivate<MainMenuScreen>();
                ScreenFactory.Instance.Activate<GameplayScreen>();
                break;
            default:

                break;
        }

    }
}
