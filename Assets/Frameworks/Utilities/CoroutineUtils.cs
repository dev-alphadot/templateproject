﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class CoroutineUtils
{
    public static IEnumerator Delay(float delayInSeconds, System.Action finishedCallback)
    {
        yield return new WaitForSeconds(delayInSeconds);
        finishedCallback();
    }

    public static IEnumerator DelayRealtime(float delayInSeconds, System.Action finishedCallback)
    {
        yield return new WaitForSecondsRealtime(delayInSeconds);
        finishedCallback();
    }

    public static IEnumerator WaitFrames(int frames, System.Action finishedCallback)
    {
        int t = 0;
        while (t < frames)
        {
            t++;
            yield return null;
        }
        finishedCallback();
    }

    public static IEnumerator WaitForEndOfFrame(System.Action finishedCallback)
    {
        yield return new WaitForEndOfFrame();
        finishedCallback();
    }

    public static IEnumerator MoveTo(Transform transform, Vector3 origin, Vector3 target, float duration, CustomEase.Type type = CustomEase.Type.LINEAR, System.Action<Vector3> onValueChanged = null, System.Action finishedCallback = null, bool clamp = true, bool unscaledTime = false)
    {
        CustomEase.EaseFunction function = CustomEase.GetEase(type);
        yield return MoveToInternal(false, transform, origin, target, duration, function, onValueChanged, finishedCallback, clamp, unscaledTime);
    }

    public static IEnumerator MoveTo(Transform transform, Vector3 origin, Vector3 target, float duration, AnimationCurve curve = null, System.Action<Vector3> onValueChanged = null, System.Action finishedCallback = null, bool clamp = true, bool unscaledTime = false)
    {
        CustomEase.EaseFunction function = CustomEase.GetEase(curve);
        yield return MoveToInternal(false, transform, origin, target, duration, function, onValueChanged, finishedCallback, clamp, unscaledTime);
    }

    public static IEnumerator MoveToLocal(Transform transform, Vector3 origin, Vector3 target, float duration, CustomEase.Type type = CustomEase.Type.LINEAR, System.Action<Vector3> onValueChanged = null, System.Action finishedCallback = null, bool clamp = true, bool unscaledTime = false)
    {
        CustomEase.EaseFunction function = CustomEase.GetEase(type);
        yield return MoveToInternal(true, transform, origin, target, duration, function, onValueChanged, finishedCallback, clamp, unscaledTime);
    }

    public static IEnumerator MoveToLocal(Transform transform, Vector3 origin, Vector3 target, float duration, AnimationCurve curve = null, System.Action<Vector3> onValueChanged = null, System.Action finishedCallback = null, bool clamp = true, bool unscaledTime = false)
    {
        CustomEase.EaseFunction function = CustomEase.GetEase(curve);
        yield return MoveToInternal(true, transform, origin, target, duration, function, onValueChanged, finishedCallback, clamp, unscaledTime);
    }

    private static IEnumerator MoveToInternal(bool isLocal, Transform transform, Vector3 origin, Vector3 target, float duration, CustomEase.EaseFunction function, System.Action<Vector3> onValueChanged = null, System.Action finishedCallback = null, bool clamp = true, bool unscaledTime = false)
    {
        float delta = 0f;
        while (delta <= duration)
        {
            delta += unscaledTime ? Time.unscaledDeltaTime : Time.deltaTime;
            float percent = clamp ? Mathf.Clamp01(delta / duration) : (delta / duration);
            float evaluation = function(percent);
            Vector3 posToMoveTo = clamp ? Vector3.Lerp(origin, target, evaluation) : Vector3.LerpUnclamped(origin, target, evaluation);
            if (isLocal)
            {
                transform.localPosition = posToMoveTo;
            }
            else
            {
                transform.position = posToMoveTo;
            }

            onValueChanged?.Invoke(posToMoveTo);

            yield return null;
        }

        if (isLocal)
        {
            transform.localPosition = target;
        }
        else
        {
            transform.position = target;
        }

        if (finishedCallback != null)
        {
            finishedCallback();
        }
    }

    public static IEnumerator ScaleTo(Transform transform, Vector3 startScale, Vector3 endScale, float duration, CustomEase.Type type = CustomEase.Type.LINEAR, System.Action<Vector3> onValueChanged = null, System.Action finishedCallback = null, bool clamp = true, bool unscaledTime = false)
    {
        CustomEase.EaseFunction function = CustomEase.GetEase(type);
        yield return ScaleToInternal(transform, startScale, endScale, duration, function, onValueChanged, finishedCallback, clamp, unscaledTime);
    }

    public static IEnumerator ScaleTo(Transform transform, Vector3 startScale, Vector3 endScale, float duration, AnimationCurve curve = null, System.Action<Vector3> onValueChanged = null, System.Action finishedCallback = null, bool clamp = true, bool unscaledTime = false)
    {
        CustomEase.EaseFunction function = CustomEase.GetEase(curve);
        yield return ScaleToInternal(transform, startScale, endScale, duration, function, onValueChanged, finishedCallback, clamp, unscaledTime);
    }

    private static IEnumerator ScaleToInternal(Transform transform, Vector3 startScale, Vector3 endScale, float duration, CustomEase.EaseFunction function, System.Action<Vector3> onValueChanged = null, System.Action finishedCallback = null, bool clamp = true, bool unscaledTime = false)
    {
        float delta = 0f;
        while (delta <= duration)
        {
            delta += unscaledTime ? Time.unscaledDeltaTime : Time.deltaTime;
            float percent = clamp ? Mathf.Clamp01(delta / duration) : (delta / duration);
            float evaluation = function(percent);
            Vector3 currentScale = clamp ? Vector3.Lerp(startScale, endScale, evaluation) : Vector3.LerpUnclamped(startScale, endScale, evaluation);

            transform.localScale = currentScale;
            onValueChanged?.Invoke(currentScale);
            yield return null;
        }

        transform.localScale = endScale;
        if (finishedCallback != null)
        {
            finishedCallback();
        }
    }

    public static IEnumerator Rotate(Transform transform, Vector3 targetRot, float rotationSpeed, CustomEase.Type type = CustomEase.Type.LINEAR, System.Action<Vector3> onValueChanged = null, System.Action finishedCallback = null, bool unscaledTime = false)
    {
        CustomEase.EaseFunction function = CustomEase.GetEase(type);
        yield return RotateInternal(transform, targetRot, rotationSpeed, function, onValueChanged, finishedCallback, unscaledTime);
    }

    public static IEnumerator Rotate(Transform transform, Vector3 targetRot, float rotationSpeed, AnimationCurve curve = null, System.Action<Vector3> onValueChanged = null, System.Action finishedCallback = null, bool unscaledTime = false)
    {
        CustomEase.EaseFunction function = CustomEase.GetEase(curve);
        yield return RotateInternal(transform, targetRot, rotationSpeed, function, onValueChanged, finishedCallback, unscaledTime);
    }

    private static IEnumerator RotateInternal(Transform transform, Vector3 targetRot, float rotationSpeed, CustomEase.EaseFunction function, System.Action<Vector3> onValueChanged = null, System.Action finishedCallback = null, bool unscaledTime = false)
    {
        Quaternion oldRotation = transform.rotation;
        Quaternion newRotation = Quaternion.Euler(targetRot);
        for (float t = 0.0f; t < 1.0; t += (unscaledTime ? Time.unscaledDeltaTime : Time.deltaTime) * rotationSpeed)
        {
            float evaluation = function(t);
            transform.rotation = Quaternion.Slerp(oldRotation, newRotation, evaluation);

            Vector3 currentRot = transform.rotation.eulerAngles;
            onValueChanged?.Invoke(currentRot);
            yield return null;
        }

        transform.rotation = newRotation;
        if (finishedCallback != null)
        {
            finishedCallback();
        }
    }

    public static IEnumerator RotateTo(Transform transform, Vector3 targetRot, float duration, CustomEase.Type type = CustomEase.Type.LINEAR, System.Action<Vector3> onValueChanged = null, System.Action finishedCallback = null, bool clamp = true, bool unscaledTime = false)
    {
        CustomEase.EaseFunction function = CustomEase.GetEase(type);
        yield return RotateToInternal(false, transform, targetRot, duration, function, onValueChanged, finishedCallback, clamp, unscaledTime);
    }

    public static IEnumerator RotateTo(Transform transform, Vector3 targetRot, float duration, AnimationCurve curve = null, System.Action<Vector3> onValueChanged = null, System.Action finishedCallback = null, bool clamp = true, bool unscaledTime = false)
    {
        CustomEase.EaseFunction function = CustomEase.GetEase(curve);
        yield return RotateToInternal(false, transform, targetRot, duration, function, onValueChanged, finishedCallback, clamp, unscaledTime);
    }

    public static IEnumerator RotateToLocal(Transform transform, Vector3 targetRot, float duration, CustomEase.Type type = CustomEase.Type.LINEAR, System.Action<Vector3> onValueChanged = null, System.Action finishedCallback = null, bool clamp = true, bool unscaledTime = false)
    {
        CustomEase.EaseFunction function = CustomEase.GetEase(type);
        yield return RotateToInternal(true, transform, targetRot, duration, function, onValueChanged, finishedCallback, clamp, unscaledTime);
    }

    public static IEnumerator RotateToLocal(Transform transform, Vector3 targetRot, float duration, AnimationCurve curve = null, System.Action<Vector3> onValueChanged = null, System.Action finishedCallback = null, bool clamp = true, bool unscaledTime = false)
    {
        CustomEase.EaseFunction function = CustomEase.GetEase(curve);
        yield return RotateToInternal(true, transform, targetRot, duration, function, onValueChanged, finishedCallback, clamp, unscaledTime);
    }

    private static IEnumerator RotateToInternal(bool isLocal, Transform transform, Vector3 targetRot, float duration, CustomEase.EaseFunction function, System.Action<Vector3> onValueChanged = null, System.Action finishedCallback = null, bool clamp = true, bool unscaledTime = false)
    {
        Quaternion startRotation = transform.rotation;

        if (isLocal)
        {
            startRotation = transform.localRotation;
        }
        Quaternion endRotation = Quaternion.Euler(targetRot);
        float time = 0.0f;
        Quaternion currentRot = Quaternion.identity;
        while (time < duration)
        {
            time += unscaledTime ? Time.unscaledDeltaTime : Time.deltaTime;
            float percent = time / duration;
            float evaluation = function(percent);
            currentRot = clamp ? Quaternion.Lerp(startRotation, endRotation, evaluation) : Quaternion.LerpUnclamped(startRotation, endRotation, evaluation);
            if (isLocal)
            {
                transform.localRotation = currentRot;
            }
            else
            {
                transform.rotation = currentRot;
            }

            onValueChanged?.Invoke(currentRot.eulerAngles);

            yield return null;
        }

        if (isLocal)
        {
            transform.localRotation = endRotation;
        }
        else
        {
            transform.rotation = endRotation;
        }

        if (finishedCallback != null)
        {
            finishedCallback();
        }
    }

    public static IEnumerator LerpFloat(float startValue, float endValue, float duration, System.Action<float> valueChangedCallback, System.Action finishedCallback = null, CustomEase.Type type = CustomEase.Type.LINEAR, bool clamp = true, bool unscaledTime = false)
    {
        CustomEase.EaseFunction function = CustomEase.GetEase(type);
        yield return LerpFloatInternal(startValue, endValue, duration, function, valueChangedCallback, finishedCallback, clamp, unscaledTime);
    }

    public static IEnumerator LerpFloat(float startValue, float endValue, float duration, System.Action<float> valueChangedCallback, System.Action finishedCallback = null, AnimationCurve curve = null, bool clamp = true, bool unscaledTime = false)
    {
        CustomEase.EaseFunction function = CustomEase.GetEase(curve);
        yield return LerpFloatInternal(startValue, endValue, duration, function, valueChangedCallback, finishedCallback, clamp, unscaledTime);
    }

    private static IEnumerator LerpFloatInternal(float startValue, float endValue, float duration, CustomEase.EaseFunction function, System.Action<float> valueChangedCallback, System.Action finishedCallback = null, bool clamp = true, bool unscaledTime = false)
    {
        float delta = 0f;
        float currentValue = 0f;
        while (delta <= duration)
        {
            delta += unscaledTime ? Time.unscaledDeltaTime : Time.deltaTime;
            float percent = clamp ? Mathf.Clamp01(delta / duration) : (delta / duration);
            float evaluation = function(percent);

            currentValue = clamp ? Mathf.Lerp(startValue, endValue, evaluation) : Mathf.LerpUnclamped(startValue, endValue, evaluation);

            valueChangedCallback(currentValue);
            yield return null;
        }

        currentValue = endValue;
        if (finishedCallback != null)
        {
            finishedCallback();
        }
    }

    public static IEnumerator LerpVector(Vector3 startValue, Vector3 endValue, float duration, System.Action<Vector3> valueChangedCallback, CustomEase.Type type = CustomEase.Type.LINEAR, System.Action finishedCallback = null, bool clamp = true, bool unscaledTime = false)
    {
        CustomEase.EaseFunction function = CustomEase.GetEase(type);
        yield return LerpVectorInternal(startValue, endValue, duration, function, valueChangedCallback, finishedCallback, clamp, unscaledTime);
    }

    public static IEnumerator LerpVector(Vector3 startValue, Vector3 endValue, float duration, System.Action<Vector3> valueChangedCallback, AnimationCurve curve = null, System.Action finishedCallback = null, bool clamp = true, bool unscaledTime = false)
    {
        CustomEase.EaseFunction function = CustomEase.GetEase(curve);
        yield return LerpVectorInternal(startValue, endValue, duration, function, valueChangedCallback, finishedCallback, clamp, unscaledTime);
    }

    private static IEnumerator LerpVectorInternal(Vector3 startValue, Vector3 endValue, float duration, CustomEase.EaseFunction function, System.Action<Vector3> valueChangedCallback, System.Action finishedCallback = null, bool clamp = true, bool unscaledTime = false)
    {
        float delta = 0f;
        Vector3 currentValue = Vector3.zero;
        while (delta <= duration)
        {
            delta += unscaledTime ? Time.unscaledDeltaTime : Time.deltaTime;
            float percent = clamp ? Mathf.Clamp01(delta / duration) : (delta / duration);
            float evaluation = function(percent);

            currentValue = clamp ? Vector3.Lerp(startValue, endValue, evaluation) : Vector3.LerpUnclamped(startValue, endValue, evaluation);


            valueChangedCallback(currentValue);
            yield return null;
        }

        currentValue = endValue;
        if (finishedCallback != null)
        {
            finishedCallback();
        }
    }

    public static IEnumerator LerpColour(Color startColour, Color endColour, float duration, System.Action<Color> valueChangedCallback, CustomEase.Type type = CustomEase.Type.LINEAR, System.Action finishedCallback = null, bool clamp = true, bool unscaledTime = false)
    {
        CustomEase.EaseFunction function = CustomEase.GetEase(type);
        yield return LerpColourInternal(startColour, endColour, duration, function, valueChangedCallback, finishedCallback, clamp, unscaledTime);
    }

    public static IEnumerator LerpColour(Color startColour, Color endColour, float duration, System.Action<Color> valueChangedCallback, AnimationCurve curve = null, System.Action finishedCallback = null, bool clamp = true, bool unscaledTime = false)
    {
        CustomEase.EaseFunction function = CustomEase.GetEase(curve);
        yield return LerpColourInternal(startColour, endColour, duration, function, valueChangedCallback, finishedCallback, clamp, unscaledTime);
    }

    private static IEnumerator LerpColourInternal(Color startColour, Color endColour, float duration, CustomEase.EaseFunction function, System.Action<Color> valueChangedCallback, System.Action finishedCallback = null, bool clamp = true, bool unscaledTime = false)
    {
        float delta = 0f;
        Color currentColour = startColour;
        while (delta <= duration)
        {
            delta += unscaledTime ? Time.unscaledDeltaTime : Time.deltaTime;
            float percent = clamp ? Mathf.Clamp01(delta / duration) : (delta / duration);
            float evaluation = function(percent);

            currentColour = clamp ? Color.Lerp(startColour, endColour, evaluation) : Color.LerpUnclamped(startColour, endColour, evaluation);

            valueChangedCallback(currentColour);
            yield return null;
        }

        currentColour = endColour;
        if (finishedCallback != null)
        {
            finishedCallback();
        }
    }

    public static IEnumerator LerpGraphicColour(Graphic graphic, Color startColour, Color endColour, float duration, CustomEase.Type type = CustomEase.Type.LINEAR, System.Action<Color> onValueChanged = null, System.Action finishedCallback = null, bool clamp = true, bool unscaledTime = false)
    {
        CustomEase.EaseFunction function = CustomEase.GetEase(type);
        System.Action<Color> updateAction = (Color color) =>
        {
            graphic.color = color;
            onValueChanged?.Invoke(color);
        };

        yield return LerpColourInternal(startColour, endColour, duration, function, updateAction, finishedCallback, clamp, unscaledTime);
    }

    public static IEnumerator LerpGraphicColour(Graphic graphic, Color startColour, Color endColour, float duration, AnimationCurve curve = null, System.Action<Color> onValueChanged = null, System.Action finishedCallback = null, bool clamp = true, bool unscaledTime = false)
    {
        CustomEase.EaseFunction function = CustomEase.GetEase(curve);
        System.Action<Color> updateAction = (Color color) =>
        {
            graphic.color = color;
            onValueChanged?.Invoke(color);
        };

        yield return LerpColourInternal(startColour, endColour, duration, function, updateAction, finishedCallback, clamp, unscaledTime);
    }

    public static IEnumerator LerpSpriteColour(SpriteRenderer sprite, Color startColour, Color endColour, float duration, CustomEase.Type type = CustomEase.Type.LINEAR, System.Action<Color> onValueChanged = null, System.Action finishedCallback = null, bool clamp = true, bool unscaledTime = false)
    {
        CustomEase.EaseFunction function = CustomEase.GetEase(type);
        System.Action<Color> updateAction = (Color color) =>
        {
            sprite.color = color;
            onValueChanged?.Invoke(color);
        };

        yield return LerpColourInternal(startColour, endColour, duration, function, updateAction, finishedCallback, clamp, unscaledTime);
    }

    public static IEnumerator LerpSpriteColour(SpriteRenderer sprite, Color startColour, Color endColour, float duration, AnimationCurve curve = null, System.Action<Color> onValueChanged = null, System.Action finishedCallback = null, bool clamp = true, bool unscaledTime = false)
    {
        CustomEase.EaseFunction function = CustomEase.GetEase(curve);
        System.Action<Color> updateAction = (Color color) =>
        {
            sprite.color = color;
            onValueChanged?.Invoke(color);
        };

        yield return LerpColourInternal(startColour, endColour, duration, function, updateAction, finishedCallback, clamp, unscaledTime);
    }

    public static IEnumerator LerpMaterialColour(Material material, Color startColour, Color endColour, float duration, CustomEase.Type type = CustomEase.Type.LINEAR, System.Action<Color> onValueChanged = null, System.Action finishedCallback = null, bool clamp = true, bool unscaledTime = false)
    {
        CustomEase.EaseFunction function = CustomEase.GetEase(type);
        System.Action<Color> updateAction = (Color color) =>
        {
            material.color = color;
            onValueChanged?.Invoke(color);
        };

        yield return LerpColourInternal(startColour, endColour, duration, function, updateAction, finishedCallback, clamp, unscaledTime);
    }

    public static IEnumerator LerpMaterialColour(Material material, Color startColour, Color endColour, float duration, AnimationCurve curve = null, System.Action<Color> onValueChanged = null, System.Action finishedCallback = null, bool clamp = true, bool unscaledTime = false)
    {
        CustomEase.EaseFunction function = CustomEase.GetEase(curve);
        System.Action<Color> updateAction = (Color color) =>
        {
            material.color = color;
            onValueChanged?.Invoke(color);
        };

        yield return LerpColourInternal(startColour, endColour, duration, function, updateAction, finishedCallback, clamp, unscaledTime);
    }

    public static IEnumerator LerpRendererColour(Renderer renderer, Color startColour, Color endColour, float duration, AnimationCurve curve = null, System.Action<Color> onValueChanged = null, System.Action finishedCallback = null, bool clamp = true, bool unscaledTime = false)
    {
        return LerpMaterialColour(renderer.material, startColour, endColour, duration, curve, onValueChanged, finishedCallback, clamp, unscaledTime);
    }

    public static IEnumerator LerpRendererColour(Renderer renderer, Color startColour, Color endColour, float duration, CustomEase.Type type, System.Action<Color> onValueChanged = null, System.Action finishedCallback = null, bool clamp = true, bool unscaledTime = false)
    {
        return LerpMaterialColour(renderer.material, startColour, endColour, duration, type, onValueChanged, finishedCallback, clamp, unscaledTime);
    }

    public static IEnumerator LerpAlpha(Color color, float startAlpha, float endAlpha, float duration, System.Action<Color> valueChangedCallback, AnimationCurve curve = null, System.Action finishedCallback = null, bool clamp = true, bool unscaledTime = false)
    {
        Color startColour = color;
        startColour.a = startAlpha;

        Color endColour = color;
        endColour.a = endAlpha;

        yield return LerpColour(startColour, endColour, duration, valueChangedCallback, curve, finishedCallback, clamp, unscaledTime);
    }

    public static IEnumerator LerpAlpha(Color color, float startAlpha, float endAlpha, float duration, System.Action<Color> valueChangedCallback, CustomEase.Type type = CustomEase.Type.LINEAR, System.Action finishedCallback = null, bool clamp = true, bool unscaledTime = false)
    {
        Color startColour = color;
        startColour.a = startAlpha;

        Color endColour = color;
        endColour.a = endAlpha;

        yield return LerpColour(startColour, endColour, duration, valueChangedCallback, type, finishedCallback, clamp, unscaledTime);
    }

    public static IEnumerator LerpAlpha(Graphic graphic, float startAlpha, float endAlpha, float duration, CustomEase.Type type = CustomEase.Type.LINEAR, System.Action<float> onValueChanged = null, System.Action finishedCallback = null, bool clamp = true, bool unscaledTime = false)
    {
        System.Action<Color> updateAction = (Color color) =>
        {
            graphic.color = color;
            onValueChanged?.Invoke(color.a);
        };

        return LerpAlpha(graphic.color, startAlpha, endAlpha, duration, updateAction, type, finishedCallback, clamp, unscaledTime);
    }

    public static IEnumerator LerpAlpha(Graphic graphic, float startAlpha, float endAlpha, float duration, AnimationCurve curve, System.Action<float> onValueChanged = null, System.Action finishedCallback = null, bool clamp = true, bool unscaledTime = false)
    {
        System.Action<Color> updateAction = (Color color) =>
        {
            graphic.color = color;
            onValueChanged?.Invoke(color.a);
        };

        return LerpAlpha(graphic.color, startAlpha, endAlpha, duration, updateAction, curve, finishedCallback, clamp, unscaledTime);
    }

    public static IEnumerator LerpAlpha(Material material, float startAlpha, float endAlpha, float duration, AnimationCurve curve = null, System.Action<float> onValueChanged = null, System.Action finishedCallback = null, bool clamp = true, bool unscaledTime = false)
    {
        System.Action<Color> updateAction = (Color color) =>
        {
            material.color = color;
            onValueChanged?.Invoke(color.a);
        };

        return LerpAlpha(material.color, startAlpha, endAlpha, duration, updateAction, curve, finishedCallback, clamp, unscaledTime);
    }

    public static IEnumerator LerpAlpha(Material material, float startAlpha, float endAlpha, float duration, CustomEase.Type type = CustomEase.Type.LINEAR, System.Action<float> onValueChanged = null, System.Action finishedCallback = null, bool clamp = true, bool unscaledTime = false)
    {
        System.Action<Color> updateAction = (Color color) =>
        {
            material.color = color;
            onValueChanged?.Invoke(color.a);
        };

        return LerpAlpha(material.color, startAlpha, endAlpha, duration, updateAction, type, finishedCallback, clamp, unscaledTime);
    }

    public static IEnumerator LerpAlpha(Renderer renderer, float startAlpha, float endAlpha, float duration, AnimationCurve curve = null, System.Action<float> onValueChanged = null, System.Action finishedCallback = null, bool clamp = true, bool unscaledTime = false)
    {
        System.Action<Color> updateAction = (Color color) =>
        {
            renderer.material.color = color;
            onValueChanged?.Invoke(color.a);
        };

        return LerpAlpha(renderer.material.color, startAlpha, endAlpha, duration, updateAction, curve, finishedCallback, clamp, unscaledTime);
    }

    public static IEnumerator LerpAlpha(Renderer renderer, float startAlpha, float endAlpha, float duration, CustomEase.Type type = CustomEase.Type.LINEAR, System.Action<float> onValueChanged = null, System.Action finishedCallback = null, bool clamp = true, bool unscaledTime = false)
    {
        System.Action<Color> updateAction = (Color color) =>
        {
            renderer.material.color = color;
            onValueChanged?.Invoke(color.a);
        };

        return LerpAlpha(renderer.material.color, startAlpha, endAlpha, duration, updateAction, type, finishedCallback, clamp, unscaledTime);
    }

    public static IEnumerator LerpAlpha(CanvasGroup canvasGroup, float startAlpha, float endAlpha, float duration, CustomEase.Type type = CustomEase.Type.LINEAR, System.Action<float> onValueChanged = null, System.Action finishedCallback = null, bool clamp = true, bool unscaledTime = false)
    {
        CustomEase.EaseFunction function = CustomEase.GetEase(type);
        System.Action<float> updateAction = (float alpha) =>
        {
            canvasGroup.alpha = alpha;
            onValueChanged?.Invoke(alpha);
        };
        return LerpFloatInternal(startAlpha, endAlpha, duration, function, updateAction, finishedCallback, clamp, unscaledTime);
    }

    public static IEnumerator LerpAlpha(CanvasGroup canvasGroup, float startAlpha, float endAlpha, float duration, AnimationCurve curve = null, System.Action<float> onValueChanged = null, System.Action finishedCallback = null, bool clamp = true, bool unscaledTime = false)
    {
        CustomEase.EaseFunction function = CustomEase.GetEase(curve);
        System.Action<float> updateAction = (float alpha) =>
        {
            canvasGroup.alpha = alpha;
            onValueChanged?.Invoke(alpha);
        };
        return LerpFloatInternal(startAlpha, endAlpha, duration, function, updateAction, finishedCallback, clamp, unscaledTime);
    }
}