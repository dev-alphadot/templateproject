﻿/// <summary>
/// All ease types are from here: https://easings.net/
/// </summary>

using UnityEngine;

	public class CustomEase
	{
		public enum Type
		{
			LINEAR,
			IN_SINE,
			OUT_SINE,
			IN_OUT_SINE,
			IN_BACK,
			OUT_BACK,
			IN_OUT_BACK,
			IN_QUAD,
			OUT_QUAD,
			IN_OUT_QUAD,
			IN_CUBIC,
			OUT_CUBIC,
			IN_OUT_CUBIC,
			IN_QUART,
			OUT_QUART,
			IN_OUT_QUART,
			IN_QUINT,
			OUT_QUINT,
			IN_OUT_QUINT,
			IN_EXPO,
			OUT_EXPO,
			IN_OUT_EXPO,
			IN_CIRC,
			OUT_CIRC,
			IN_OUT_CIRC,
			IN_ELASTIC,
			OUT_ELASTIC,
			IN_OUT_ELASTIC,
			IN_BOUNCE,
			OUT_BOUNCE,
			IN_OUT_BOUNCE,
			NONE
		}

		public delegate float EaseFunction(float value);

		public static EaseFunction GetEase(Type type)
		{
			switch (type)
			{
				case Type.LINEAR:
					return Linear;
				case Type.IN_SINE:
					return EaseInSine;
				case Type.OUT_SINE:
					return EaseOutSine;
				case Type.IN_OUT_SINE:
					return EaseInOutSine;
				case Type.IN_BACK:
					return EaseInBack;
				case Type.OUT_BACK:
					return EaseOutBack;
				case Type.IN_OUT_BACK:
					return EaseInOutBack;
				case Type.IN_QUAD:
					return EaseInQuad;
				case Type.OUT_QUAD:
					return EaseOutQuad;
				case Type.IN_OUT_QUAD:
					return EaseInOutQuad;
				case Type.IN_CUBIC:
					return EaseInCubic;
				case Type.OUT_CUBIC:
					return EaseOutCubic;
				case Type.IN_OUT_CUBIC:
					return EaseInOutCubic;
				case Type.IN_QUART:
					return EaseInQuart;
				case Type.OUT_QUART:
					return EaseOutQuart;
				case Type.IN_OUT_QUART:
					return EaseInOutQuart;
				case Type.IN_QUINT:
					return EaseInQuint;
				case Type.OUT_QUINT:
					return EaseOutQuint;
				case Type.IN_OUT_QUINT:
					return EaseInOutQuint;
				case Type.IN_EXPO:
					return EaseInExpo;
				case Type.OUT_EXPO:
					return EaseInExpo;
				case Type.IN_OUT_EXPO:
					return EaseInOutExpo;
				case Type.IN_CIRC:
					return EaseInCirc;
				case Type.OUT_CIRC:
					return EaseOutCirc;
				case Type.IN_OUT_CIRC:
					return EaseInOutCirc;
				case Type.IN_ELASTIC:
					return EaseInElastic;
				case Type.OUT_ELASTIC:
					return EaseOutElastic;
				case Type.IN_OUT_ELASTIC:
					return EaseInOutElastic;
				case Type.IN_BOUNCE:
					return EaseInBounce;
				case Type.OUT_BOUNCE:
					return EaseOutBounce;
				case Type.IN_OUT_BOUNCE:
					return EaseInOutBounce;
				default:
					return Linear;
			}
		}

		private const float IN_BACK_C1 = 1.70158f;
		private const float IN_BACK_C3 = IN_BACK_C1 + 1;

		private const float OUT_BACK_C1 = 1.70158f;
		private const float OUT_BACK_C3 = OUT_BACK_C1 + 1;

		private const float IN_OUT_BACK_C1 = 1.70158f;
		private const float IN_OUT_BACK_C2 = IN_OUT_BACK_C1 * 1.525f;

		private const float OUT_BOUNCE_N1 = 7.5625f;
		private const float OUT_BOUNCE_D1 = 2.75f;

		public static EaseFunction GetEase(AnimationCurve curve)
		{
			if (curve != null)
			{
				EaseFunction f = delegate (float t)
				{
					return curve.Evaluate(t);
				};

				return f;
			}
			else
			{
				return GetEase(Type.LINEAR);
			}

		}

		public static float Linear(float x)
		{
			return x;
		}

		public static float EaseInSine(float x)
		{
			return 1 - Mathf.Cos((x * Mathf.PI) / 2);
		}

		public static float EaseOutSine(float x)
		{
			return Mathf.Sin((x * Mathf.PI) / 2);
		}

		public static float EaseInOutSine(float x)
		{
			return -(Mathf.Cos(Mathf.PI * x) - 1) / 2;
		}

		public static float EaseInBack(float x)
		{
			return IN_BACK_C3 * x * x * x - IN_BACK_C1 * x * x;
		}

		public static float EaseOutBack(float x)
		{
			return 1 + OUT_BACK_C3 * Mathf.Pow(x - 1, 3) + OUT_BACK_C1 * Mathf.Pow(x - 1, 2);
		}

		public static float EaseInOutBack(float x)
		{
			return x < 0.5
				? (Mathf.Pow(2 * x, 2) * ((IN_OUT_BACK_C2 + 1) * 2 * x - IN_OUT_BACK_C2)) / 2
				: (Mathf.Pow(2 * x - 2, 2) * ((IN_OUT_BACK_C2 + 1) * (x * 2 - 2) + IN_OUT_BACK_C2) + 2) / 2;
		}

		public static float EaseInQuad(float x)
		{
			return x * x;
		}

		public static float EaseOutQuad(float x)
		{
			return 1 - (1 - x) * (1 - x);
		}

		public static float EaseInOutQuad(float x)
		{
			return x < 0.5f
				? 2 * x * x
				: 1 - Mathf.Pow(-2 * x + 2, 2) / 2;
		}

		public static float EaseInCubic(float x)
		{
			return x * x * x;
		}

		public static float EaseOutCubic(float x)
		{
			return 1 - Mathf.Pow(1 - x, 3);
		}

		public static float EaseInOutCubic(float x)
		{
			return x < 0.5f
				? 4 * x * x * x
				: 1 - Mathf.Pow(-2 * x + 2, 3) / 2;
		}

		public static float EaseInQuart(float x)
		{
			return x * x * x * x;
		}

		public static float EaseOutQuart(float x)
		{
			return 1 - Mathf.Pow(1 - x, 4);
		}

		public static float EaseInOutQuart(float x)
		{
			return x < 0.5
				? 8 * x * x * x * x
				: 1 - Mathf.Pow(-2 * x + 2, 4) / 2;
		}

		public static float EaseInQuint(float x)
		{
			return x * x * x * x * x;
		}

		public static float EaseOutQuint(float x)
		{
			return 1 - Mathf.Pow(1 - x, 5);
		}

		public static float EaseInOutQuint(float x)
		{
			return x < 0.5
				? 16 * x * x * x * x * x
				: 1 - Mathf.Pow(-2 * x + 2, 5) / 2;
		}

		public static float EaseInExpo(float x)
		{
			return x == 0
				? 0
				: Mathf.Pow(2, 10 * x - 10);
		}

		public static float EaseOutExpo(float x)
		{
			return x == 1
				? 1
				: 1 - Mathf.Pow(2, -10 * x);
		}

		public static float EaseInOutExpo(float x)
		{
			return x == 0
			? 0
			: x == 1
				? 1
				: x < 0.5 ? Mathf.Pow(2, 20 * x - 10) / 2
			: (2 - Mathf.Pow(2, -20 * x + 10)) / 2;
		}

		public static float EaseInCirc(float x)
		{
			return 1 - Mathf.Sqrt(1 - Mathf.Pow(x, 2));
		}

		public static float EaseOutCirc(float x)
		{
			return Mathf.Sqrt(1 - Mathf.Pow(x - 1, 2));
		}

		public static float EaseInOutCirc(float x)
		{
			return x < 0.5
				? (1 - Mathf.Sqrt(1 - Mathf.Pow(2 * x, 2))) / 2
				: (Mathf.Sqrt(1 - Mathf.Pow(-2 * x + 2, 2)) + 1) / 2;
		}

		public static float EaseInElastic(float x)
		{
			float c4 = (2 * Mathf.PI) / 3;

			return x == 0
			  ? 0
			  : x == 1
				? 1
				: -Mathf.Pow(2, 10 * x - 10) * Mathf.Sin((x * 10f - 10.75f) * c4);
		}

		public static float EaseOutElastic(float x)
		{
			float c4 = (2 * Mathf.PI) / 3;

			return x == 0
			  ? 0
			  : x == 1
				? 1
				: Mathf.Pow(2, -10 * x) * Mathf.Sin((x * 10f - 0.75f) * c4) + 1;
		}

		public static float EaseInOutElastic(float x)
		{
			float c5 = (2 * Mathf.PI) / 4.5f;

			return x == 0
			  ? 0
			  : x == 1
				? 1
				: x < 0.5
					? -(Mathf.Pow(2, 20 * x - 10) * Mathf.Sin((20 * x - 11.125f) * c5)) / 2
					: (Mathf.Pow(2, -20 * x + 10) * Mathf.Sin((20 * x - 11.125f) * c5)) / 2 + 1;
		}

		public static float EaseInBounce(float x)
		{
			return 1 - EaseOutBounce(1 - x);
		}

		public static float EaseOutBounce(float x)
		{

			if (x < 1 / OUT_BOUNCE_D1)
			{
				return OUT_BOUNCE_N1 * x * x;
			}
			else if (x < 2 / OUT_BOUNCE_D1)
			{
				return OUT_BOUNCE_N1 * (x -= 1.5f / OUT_BOUNCE_D1) * x + 0.75f;
			}
			else if (x < 2.5 / OUT_BOUNCE_D1)
			{
				return OUT_BOUNCE_N1 * (x -= 2.25f / OUT_BOUNCE_D1) * x + 0.9375f;
			}
			else
			{
				return OUT_BOUNCE_N1 * (x -= 2.625f / OUT_BOUNCE_D1) * x + 0.984375f;
			}
		}

		public static float EaseInOutBounce(float x)
		{
			return x < 0.5
				? (1 - EaseOutBounce(1 - 2 * x)) / 2
				: (1 + EaseOutBounce(2 * x - 1)) / 2;
		}
	}