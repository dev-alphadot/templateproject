﻿using UnityEngine;

namespace Singleton
{
    public class GenericSingleton<T> : MonoBehaviour where T : Component
    {
        private static T instance;
        public static T Instance
        {
            get
            {
                instance = instance ?? FindObjectOfType<T>();
                if (instance == null)
                {
                    GameObject obj = new GameObject(typeof(T).Name);
                    instance = obj.AddComponent<T>();
                }
                return instance;
            }
        }
        [SerializeField] private bool dontDestroyOnLoad;

        protected virtual void Awake()
        {
            if (instance == null)
            {
                if (dontDestroyOnLoad)
                    DontDestroyOnLoad(this.gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }
}
