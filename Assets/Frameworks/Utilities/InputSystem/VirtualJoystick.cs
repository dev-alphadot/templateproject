﻿using UnityEngine;


namespace KWGameplay
{
    public interface IJoyControllable
    {
        void ControlChanged(Vector2 offset);
        void ControlStop();
        void ControlStart();
    }
    [HelpURL("http://wiki.kwalee.local/index.php?title=Virtual_Joystick")]
    public class VirtualJoystick : MonoBehaviour
    {
        [Header("Joystick \"lenght\" in screen terms [0,1], the joystick center will follow the finger passed this distance.")]
        [SerializeField] private float m_followDistance = 0.3f;
        [Header("Joystick dead area in screen terms [0,1]")]
        [SerializeField] private float m_deadZone = 0.05f;
        [Header("Factor multiplier for the output (optional)")]
        [SerializeField] private float m_factor = 1f;
        [Header("Canvas (optional)")]
        [SerializeField] private Canvas m_canvas;

        private IJoyControllable m_controllable;
        private GameObject m_uiVisuals;

        private bool m_mouseDown;
        private Vector3 m_initialMousePress;
        private bool m_moving;

        private bool m_useFixedUpdate;

        public void RegisterControllable(IJoyControllable controllable, bool useFixedUpdate = false)
        {
            m_controllable = controllable;
            m_useFixedUpdate = useFixedUpdate;
        }

        public void DeregisterControllble()
        {
            m_controllable = null;
        }

        public void RegisterUIVisuals(GameObject uiVIsual)
        {
            m_uiVisuals = uiVIsual;
        }

        private void Start()
        {
            m_controllable = GetComponent<IJoyControllable>();
        }

        private void OnEnable()
        {
            TouchArea.Input += TouchArea_Input;
        }

        private void OnDisable()
        {
            if (m_mouseDown)
            {
                m_controllable.ControlStop();
            }

            TouchArea.Input -= TouchArea_Input;
            m_mouseDown = false;
            m_moving = false;
        }

        private void OnDestroy()
        {
            if (m_mouseDown)
            {
                m_controllable.ControlStop();
            }
        }

        private void TouchArea_Input(TouchArea.PressType inputType)
        {
            if (m_controllable != null)
            {
                if (inputType == TouchArea.PressType.DOWN)
                {
                    m_mouseDown = true;
                    m_controllable.ControlStart();
                    m_initialMousePress = Input.mousePosition;
                    UIVisualSet();
                }
                else
                {
                    m_mouseDown = false;
                    m_moving = false;
                    m_controllable.ControlStop();
                    UIVisualHide();
                }
            }
        }

        private void Update()
        {
            if (!m_useFixedUpdate)
            {
                UpdateJoystick();
            }
        }

        private void FixedUpdate()
        {
            if (m_useFixedUpdate)
            {
                UpdateJoystick();
            }
        }

        private void UpdateJoystick()
        {
            if (m_mouseDown && m_controllable != null)
            {
                Vector3 offset = (Input.mousePosition - m_initialMousePress);
                offset /= Screen.width;

                float magnitude = offset.magnitude;
                if (magnitude > m_deadZone)
                {
                    m_moving = true;
                }
                if (m_moving)
                {
                    if (magnitude > m_followDistance)
                    {
                        Vector3 normalizedOffset = offset.normalized;
                        offset = normalizedOffset * m_followDistance;
                        m_initialMousePress = Input.mousePosition - (normalizedOffset * m_followDistance * Screen.width);
                        UIVisualSet();
                    }

                    Vector3 factorizedOffset = offset * m_factor;
                    m_controllable.ControlChanged(new Vector2(factorizedOffset.x, factorizedOffset.y));
                }
            }
        }

        private void UIVisualSet()
        {
            if (m_uiVisuals != null)
            {
                m_uiVisuals.SetActive(true);
                Vector3 canvasPos = CanvasPositioningExtensions.ScreenToCanvasPosition(m_canvas, m_initialMousePress);
                m_uiVisuals.transform.localPosition = canvasPos;
            }
        }

        private void UIVisualHide()
        {
            if (m_uiVisuals != null)
            {
                m_uiVisuals.SetActive(false);
            }

        }
    }
}