﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

    public class TouchArea : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        [SerializeField] bool m_workInScreenTransition;
        private static bool s_pressed;

        public enum PressType
        {
            DOWN,
            UP
        }

        public delegate void InputEvent(PressType press);

        public static event InputEvent Input;
        public static void SendInputEvent(PressType press)
        {
            if (Input != null)
            {
                Input(press);
            }
        }


        public void OnPointerDown(PointerEventData eventData)
        {
            s_pressed = true;
            SendInputEvent(PressType.DOWN);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            s_pressed = false;
            SendInputEvent(PressType.UP);
        }

        private void Update()
        {
            if (m_workInScreenTransition)
            {
                if (s_pressed && UnityEngine.Input.GetMouseButtonUp(0))
                {
                    s_pressed = false;
                    SendInputEvent(PressType.UP);
                    m_workInScreenTransition = false;
                }  
            }
        }
    }
