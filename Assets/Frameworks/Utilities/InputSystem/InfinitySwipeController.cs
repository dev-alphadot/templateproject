﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public interface IInfinityControllable
{
    void ControlChanged(Vector2 offset);
    void ControlStop();
    void ControlStart();
}
public class InfinitySwipeController : MonoBehaviour
{
      [Header("Swipe dead area in screen terms [0,1]")]
        [SerializeField] private float m_deadZone = 0.05f;
        [Header("Factor multiplier for the output (optional)")]
        [SerializeField] private float m_factor = 1f;

        private IInfinityControllable m_controllable;

        private bool m_mouseDown;
        private Vector3 m_initialMousePress;
        private bool m_moving;

        public void RegisterControllable(IInfinityControllable controllable)
        {
            m_controllable = controllable;
        }

        public void DeregisterControllble()
        {
            m_controllable = null;
        }

        void Awake()
        {
            TouchArea.Input += TouchArea_Input;
        }

        void OnDestroy()
        {
            TouchArea.Input -= TouchArea_Input;
        }

        private void TouchArea_Input(TouchArea.PressType inputType)
        {
            if (m_controllable != null)
            {
                if (inputType == TouchArea.PressType.DOWN)
                {
                    m_mouseDown = true;
                    m_controllable.ControlStart();
                    m_initialMousePress = Input.mousePosition;
                }
                else
                {
                    m_mouseDown = false;
                    m_moving = false;
                    m_controllable.ControlStop();
                }
            }
        }

        public void ResetSwipe()
        {
            if (m_mouseDown)
            {
                m_initialMousePress = Input.mousePosition;
            }
        }

        private void Update()
        {
            if (m_mouseDown && m_controllable != null)
            {
                Vector3 offset = (Input.mousePosition - m_initialMousePress);
                offset /= Screen.width;

                float magnitude = offset.magnitude;
                if (magnitude > m_deadZone)
                {
                    m_moving = true;
                }
                if (m_moving)
                {
                    Vector3 factorizedOffset = offset * m_factor;
                    m_controllable.ControlChanged(new Vector2(factorizedOffset.x, factorizedOffset.y));
                }
            }
        }
}
