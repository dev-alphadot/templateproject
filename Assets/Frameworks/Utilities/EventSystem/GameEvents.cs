﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class GameEvents
{
    public delegate void BasicDelegate();
    public delegate void IntDelegate(int integer);
    public delegate void StringDelegate(string text);
    public delegate void BoolDelegate(bool boolean);
    public delegate void FloatDelegate(float floatValue);
    public delegate void LongDelegate(long longInteger);

    public static event BasicDelegate LevelCompletedEvent;
    public static void SendLevelCompleteEvent()
    {
        LevelCompletedEvent?.Invoke();
    }
    public static event BasicDelegate GameStartEvent;
    public static void SendGameStartEvent()
    {
        GameStartEvent?.Invoke();
    }
    public static event BasicDelegate GameOverEvent;
    public static void SendGameOverEvent()
    {
        GameOverEvent?.Invoke();
    }
}
