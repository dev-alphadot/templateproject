﻿using System.Collections.Generic;

interface IAbstractIterator<T>
{
    T GetNext(bool isClockwise);
}
public class CyclicIterator<T> : IAbstractIterator<T> where T : class
{

    private List<T> items;
    private int _currentIndex = -1, _indexStart = -1;

    private bool isDone;

    public bool IsDone
    {
        get { return isDone; }
    }

    public void SetStartingIndex(List<T> objs, System.Predicate<T> match)
    {
        this.items = objs;
        _indexStart = _currentIndex = items.FindIndex(match);
        UnityEngine.Debug.LogFormat(" objs COUNT : {0}", items.Count);
        UnityEngine.Debug.LogFormat("<color=green> _currentIndex : {0}  => _indexStart : {1} </color=green>", _currentIndex, _indexStart);

        isDone = false;
    }

    public void ResetIterator()
    {
        _currentIndex = -1;
        _indexStart = -1;
        isDone = false;
    }

    public void ResetCycle()
    {
        _currentIndex = _indexStart;
        isDone = false;
    }

    public T GetNext(bool isClockwise)
    {
        var obj = default(T);
        _currentIndex = isClockwise ? ((_currentIndex >= items.Count - 1) ? 0 : _currentIndex + 1) :
                                      ((_currentIndex <= 0) ? items.Count - 1 : _currentIndex - 1);
        if (items[_currentIndex] != null)
        {
            isDone = _indexStart == _currentIndex;
            obj = items[_currentIndex];
        }
        return obj;
    }

}
