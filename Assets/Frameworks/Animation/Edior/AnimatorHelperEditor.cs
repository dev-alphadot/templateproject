﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using UnityEditor.Animations;

[CustomEditor(typeof(AnimatorHelper), true)]
public class AnimatorHelperEditor : Editor
{
	string animations = "Click button";

	public override void OnInspectorGUI()
	{
		AnimatorHelper element = (AnimatorHelper)target;

		DrawDefaultInspector();

		if (GUILayout.Button("Auto read animations"))
		{
			GetAnimations();
		}
		if (GUILayout.Button("Generate code"))
		{
			List<AnimatorHelper.UIAnimState> animationsList = element.GetAnimationList();
			animations = "";
			for (int i = 0; i < animationsList.Count; ++i)
			{
				string animation = animationsList[i].animationName;
				animations += "private const string ANIM_" + animation.ToUpper().Replace('-','_') + " = \"" + animation + "\"; \n";
			}
		}

		if (GUILayout.Button("Hide code"))
		{
			animations = "Hello!";
		}

		EditorGUILayout.TextArea(animations);
	}

	void GetAnimations()
	{
		AnimatorHelper element = (AnimatorHelper)target;
		Animator animator = element.GetComponent<Animator>();
		AnimatorController ac = animator.runtimeAnimatorController as AnimatorController;
		if (ac == null)
		{
			AnimatorOverrideController animatorOverrideController;
			animatorOverrideController = new AnimatorOverrideController (animator.runtimeAnimatorController);
			ac = animatorOverrideController.runtimeAnimatorController as AnimatorController;
		}

		if (ac != null)
		{
			AnimatorControllerLayer[] layers = ac.layers;
			foreach (AnimatorControllerLayer layer in layers)
			{
				AnimatorStateMachine sm = layer.stateMachine;
				ChildAnimatorState[] states = sm.states;
				for (int i = 0; i < states.Length; ++i)
				{
					element.AddState(states[i].state.name);
				}
			}

			ReloadPreviewInstances();
		}
		EditorUtility.SetDirty(element);
	}
}
#endif