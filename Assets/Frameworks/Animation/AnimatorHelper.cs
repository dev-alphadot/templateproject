﻿using UnityEngine;
using System;
using System.Collections.Generic;
public class AnimatorHelper : MonoBehaviour
{
    [System.Serializable]
    public class UIAnimState
    {
        public string animationName;
        public bool isPositionRelative;
        public bool overrideFirstPosition;

        [System.NonSerialized]
        public int animationNameHash = -1;

        public int GetAnimNameHash()
        {
            if (animationNameHash == -1)
            {
                animationNameHash = Animator.StringToHash(animationName);
            }

            return animationNameHash;
        }
    }

    [SerializeField] private List<UIAnimState> m_animStates = new List<UIAnimState>();
    [SerializeField] private Animator m_animator;
    [SerializeField] private bool m_useFirstFrameHack = false;
    [SerializeField] private bool m_continueAnimationOnEnable = true;
    [SerializeField] private bool m_turnOnAnimatorOnEnable = true;

    private Vector3 m_startPosition = Vector3.zero;
    private bool m_animStarted;
    private Action m_animFinishedCallback = null;
    private UIAnimState m_activeUIAnimState;
    private UIAnimState m_incomingUIAnimState;
    private bool m_activeAnimHasFinished = true;
    private float m_currentClipTime = 0f;
    private bool m_isOverrideFirstPosition;
    private bool m_isRelativePosition;
    private int m_layer;

    private void Awake()
    {
        if (m_animator == null)
        {
            m_animator = GetComponent<Animator>();
        }
    }

    private void Start()
    {
        if (m_useFirstFrameHack)
        {
            m_animator.Update(0.02f);
        }
    }

    private void OnEnable()
    {
        if (m_animator != null)
        {
            m_animator.enabled = m_turnOnAnimatorOnEnable;

            if (m_activeUIAnimState != null && m_continueAnimationOnEnable)
            {
                m_animator.Play(m_activeUIAnimState.animationName, m_layer, m_currentClipTime);
            }
        }
    }

    private void OnDisable()
    {
        if (m_animator != null)
        {
            m_animator.enabled = false;
        }
    }

    public List<UIAnimState> GetAnimationList()
    {
        return m_animStates;
    }

    public string GetCurrentAnim()
    {
        if (m_activeUIAnimState != null)
        {
            return m_activeUIAnimState.animationName;
        }

        return "";
    }

    public void AddState(string name)
    {
        foreach (UIAnimState state in m_animStates)
        {
            if (state.animationName == name)
            {
                return;
            }
        }

        UIAnimState newState = new UIAnimState();
        newState.animationName = name;

        m_animStates.Add(newState);
    }

    public void SetAnimator(Animator animator)
    {
        m_animator = animator;
    }

    public Animator GetAnimator()
    {
        return m_animator;
    }


    public void ChangeAnimaState(int index, string animationName)
    {
        UIAnimState animState = m_animStates[index];
        animState.animationName = animationName;
        animState.animationNameHash = -1;
    }

    private void PlayAnimationInternal(UIAnimState state, Action onFinished = null, float startTime = 0, float crossfade = 0, int layer = 0)
    {
        if (state == null)
        {
            if (onFinished != null)
            {
                onFinished();
            }

            return;
        }

        m_incomingUIAnimState = state;

        m_animFinishedCallback = onFinished;

        m_startPosition = this.transform.localPosition;
        m_isOverrideFirstPosition = state.overrideFirstPosition;
        m_isRelativePosition = state.isPositionRelative;

        m_layer = layer;

        if (crossfade > 0.0f)
        {
            m_animator.CrossFade(state.animationName, crossfade);
        }
        else
        {
            m_animator.Play(state.animationName, layer, startTime);
        }

        m_currentClipTime = 0;

        m_animStarted = false;

    }

    private void SetupNewActiveAnimState(UIAnimState state)
    {
        m_activeUIAnimState = state;
        m_isOverrideFirstPosition = state.overrideFirstPosition;
        m_isRelativePosition = state.isPositionRelative;
        m_activeAnimHasFinished = false;

        m_animStarted = true;
    }

    private void Update()
    {
        if (ShouldUpdateAnimState())
        {
            UpdateAnimState();
        }
    }

    private bool ShouldUpdateAnimState()
    {
        return m_animator != null;
    }

    public void PlayAnimation(string animationName, Action onFinished = null, float startTime = 0f, bool checkIsPlayingFirst = false, float crossFade = 0f, int layer = 0)
    {
        if (!m_animator.enabled)
        {
            Debug.LogError("Trying to play animation: " + animationName + ", but animator is disabled");
        }
        else
        {
            //Shoud we check if the next animation to play is ALREADY playing?
            if (checkIsPlayingFirst)
            {
                Animator anim = GetAnimator();
                AnimatorStateInfo animInfo = anim.GetCurrentAnimatorStateInfo(layer);
                bool isCurentAnimPlating = animInfo.IsName(animationName);

                if (isCurentAnimPlating)
                {
                    Debug.LogError("Trying to play animation: " + animationName + ", but it is alreay playing");
                    return;
                }
            }

            if (gameObject.activeSelf && gameObject.activeInHierarchy)
            {
                UIAnimState animState = GetAnimStateFromAnimationName(animationName);
                Debug.Assert(animState != null, "The animation " + animationName + " trying to be played in (" + gameObject.name + ") doesn't exist");
                if (animState != null)
                {
                    PlayAnimationInternal(animState, onFinished, startTime, crossFade, layer);
                }
            }
            else
            {
                Debug.LogError("Trying to play animation: " + animationName + ", but game object is not activeSelf or InHierachy");
            }
        }
    }

    private void AnimationFinished()
    {
        if (m_animFinishedCallback != null)
        {
            System.Action action = m_animFinishedCallback;
            m_animFinishedCallback = null;

            action();
        }
    }

    public bool HasAnimationStateWithName(string stateName)
    {
        return GetAnimStateFromAnimationName(stateName) != null;
    }

    private UIAnimState GetAnimStateFromAnimationName(string animationName)
    {
        foreach (UIAnimState animState in m_animStates)
        {
            if (animState.animationName == animationName)
            {
                return animState;
            }
        }

        return null;
    }

    private UIAnimState GetAnimStateFromAnimationNameHash(int animationNameHash)
    {
        foreach (UIAnimState animState in m_animStates)
        {
            if (animState.GetAnimNameHash() == animationNameHash)
            {
                return animState;
            }
        }

        return null;
    }

    private void LateUpdate()
    {
        if (m_isRelativePosition)
        {
            transform.localPosition += m_startPosition;
        }
        else if (m_isOverrideFirstPosition)
        {
            float currentClipTime = m_animStarted ? m_animator.GetCurrentAnimatorStateInfo(m_layer).normalizedTime : 0;
            Vector3 posDifference = (m_startPosition - transform.localPosition);
            transform.localPosition += Vector3.Lerp(posDifference, Vector3.zero, currentClipTime);
        }
    }

    private void UpdateAnimState()
    {
        AnimatorStateInfo stateInfo = m_animator.GetCurrentAnimatorStateInfo(m_layer);

        m_currentClipTime = stateInfo.normalizedTime;

        if (m_incomingUIAnimState != null)
        {
            if (stateInfo.shortNameHash == m_incomingUIAnimState.GetAnimNameHash())
            {
                SetupNewActiveAnimState(m_incomingUIAnimState);

                m_incomingUIAnimState = null;
            }
        }
        else
        {
            UpdateAnimFinished(stateInfo);
            UpdateAnimStarted(stateInfo);
        }
    }

    private void UpdateAnimStarted(AnimatorStateInfo stateInfo)
    {
        int animStateShortNameHash = stateInfo.shortNameHash;
        int currentAnimStateShortNameHash = m_activeUIAnimState != null ? m_activeUIAnimState.GetAnimNameHash() : -1;

        if (animStateShortNameHash != currentAnimStateShortNameHash)
        {
            UIAnimState uiState = GetAnimStateFromAnimationNameHash(animStateShortNameHash);
            if (uiState != null)
            {
                SetupNewActiveAnimState(uiState);
            }
        }
    }

    private void UpdateAnimFinished(AnimatorStateInfo stateInfo)
    {
        int animStateShortNameHash = stateInfo.shortNameHash;
        int currentAnimStateShortNameHash = m_activeUIAnimState != null ? m_activeUIAnimState.GetAnimNameHash() : -1;

        if (!m_activeAnimHasFinished)
        {
            AnimatorClipInfo[] animClipInfos = m_animator.GetCurrentAnimatorClipInfo(m_layer);
            bool animClipHasFrames = animClipInfos.Length > 0 && animClipInfos[0].clip.length <= Mathf.Epsilon;
            if (stateInfo.normalizedTime >= 1f || animStateShortNameHash != currentAnimStateShortNameHash || animClipHasFrames)
            {
                m_activeAnimHasFinished = true;


                AnimationFinished();
            }
        }
    }

    public void SetAnimatorSpeed(float speed)
    {
        m_animator.speed = speed;
    }

    public void StopAnimator()
    {
        m_animator.enabled = false;
    }

}